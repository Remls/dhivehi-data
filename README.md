# Dhivehi Data

Sample data in Thaana for use in other projects.

## countries_list.csv

This file contains a simple list of countries in Thaana.

The file has 4 columns:

| name | name_dv | country_code | dial_code |
| --- | --- | --- | --- |
| English name of the country | Dhivehi name of the country | ISO 3166-1 alpha-2 code | Calling code |
